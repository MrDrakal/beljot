package com.example.adrak.beljot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private Button btnSUp;
    private EditText editTextUser;
    private EditText editTextPass;
    private EditText editTextConfPass;
    private ProgressDialog nDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        btnSUp = (Button) findViewById(R.id.btnSignUp);
        editTextUser = (EditText) findViewById(R.id.editTextUser);
        editTextPass = (EditText) findViewById(R.id.editTextPass);
        editTextConfPass = (EditText) findViewById(R.id.editTextConfPass);

        btnSUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });
    }

    private void signUp() {
        String username = editTextUser.getText().toString().trim();
        String password = editTextPass.getText().toString().trim();
        String confPass = editTextConfPass.getText().toString().trim();

        if (TextUtils.isEmpty(username) || !Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
            Toast.makeText(this, "Please enter your Username", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(confPass)) {
            Toast.makeText(this, "Please enter your confirmation password", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!password.equals(confPass)) {
            Toast.makeText(this, "Password and confirm password do not match", Toast.LENGTH_SHORT).show();
            return;
        }

        showProgressDialog();
        // create firebase user in authentication
        Task<AuthResult> result = firebaseAuth.createUserWithEmailAndPassword(username, password);

        result.addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                if(authResult.getUser() != null){
                    onSignUpSuccess();
                }
                else {
                    OnSignUpFail();
                }
            }
        });

        result.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                OnSignUpFail();
            }
        });
    }

    private void onSignUpSuccess(){

        Toast.makeText(this, "User Created", Toast.LENGTH_LONG).show();
        dismissProgressDialog();

        // Redirect user to login
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }

    private void OnSignUpFail(){
        Toast.makeText(this, "Sign up failed", Toast.LENGTH_LONG).show();
        dismissProgressDialog();
    }

    private void showProgressDialog(){
        nDialog = new ProgressDialog(this);
        nDialog.setMessage("Loading..");
        nDialog.setIndeterminate(false);
        nDialog.setCancelable(true);
        nDialog.show();
    }

    private void dismissProgressDialog(){
        nDialog.dismiss();
    }
}
