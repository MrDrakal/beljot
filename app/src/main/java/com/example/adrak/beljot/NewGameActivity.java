package com.example.adrak.beljot;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewGameActivity extends AppCompatActivity {

    private int x = 0;
    public static final int REQUEST_CODE_Bonus = 50;
    public static final int REQUEST_CODE_Points = 40;
    public static final int REQUEST_CODE_Names = 60;

    private int teamOneTotal = 0;
    private int teamTwoTotal = 0;
    private TextView teamOnePlayers1;
    private TextView teamOnePlayers2;
    private TextView teamTwoPlayers1;
    private TextView teamTwoPlayers2;
    private String player1Team1;
    private String player2Team1;
    private String player1Team2;
    private String player2Team2;
    private String player1Team1Uid;
    private String player2Team1Uid;
    private String player1Team2Uid;
    private String player2Team2Uid;

    private HashMap<String, String> playerNamesMap = new HashMap<String, String>();

    private TextView textViewT1Total;
    private TextView textViewT2Total;


    private int teamOnePoints = 0;
    private int teamTwoPoints = 0;

    private int teamOneBonus=0;
    private int teamTwoBonus=0;

    private Button btnBonus;
    private Button btnPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);

        Intent intent = new Intent(getApplicationContext(), ChooseNameActivity.class);
        startActivityForResult(intent, REQUEST_CODE_Names);

        textViewT1Total = findViewById(R.id.textViewT1Total);
        textViewT2Total = findViewById(R.id.textViewT2Total);
        teamOnePlayers1 = findViewById(R.id.teamOnePlayer1);
        teamOnePlayers2 = findViewById(R.id.teamOnePlayer2);
        teamTwoPlayers1 = findViewById(R.id.teamTwoPlayer1);
        teamTwoPlayers2 = findViewById(R.id.teamTwoPlayer2);


        btnBonus = findViewById(R.id.btnBonus);

        btnBonus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BonusActivity.class);
                startActivityForResult(intent, REQUEST_CODE_Bonus);

            }
        });

        btnPoints = findViewById(R.id.btnPoints);

        btnPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GamePointsActivity.class);
                startActivityForResult(intent, REQUEST_CODE_Points);

            }
        });
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    private void doToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE_Names){
            if (resultCode == RESULT_OK){
                player1Team1 = data.getStringExtra("Team One Player One");
                player2Team1 = data.getStringExtra("Team One Player Two");
                player1Team2 = data.getStringExtra("Team Two Player One");
                player2Team2 = data.getStringExtra("Team Two Player Two");

                player1Team1Uid = data.getStringExtra("Team One Player One Uid");
                player2Team1Uid = data.getStringExtra("Team One Player Two Uid");
                player1Team2Uid = data.getStringExtra("Team Two Player One Uid");
                player2Team2Uid = data.getStringExtra("Team Two Player Two Uid");

                teamOnePlayers1.setText(player1Team1);
                teamOnePlayers2.setText(player2Team1);
                teamTwoPlayers1.setText(player1Team2);
                teamTwoPlayers2.setText(player2Team2);
            }
        }

         if (requestCode == REQUEST_CODE_Bonus) {
            if (resultCode == RESULT_OK) {

                teamOneBonus = data.getIntExtra("Team One Bonus", 0);
                teamTwoBonus = data.getIntExtra("Team Two Bonus", 0);

                teamOneTotal = teamOneTotal + teamOneBonus;
                teamTwoTotal = teamTwoTotal + teamOneBonus;

                int num = Integer.parseInt(textViewT1Total.getText().toString());
                num = num + teamOneBonus;
                textViewT1Total.setText(String.valueOf(num));

                int num2 = Integer.parseInt(textViewT2Total.getText().toString());
                num2 = num2 + teamTwoBonus;
                if (num >= 1001) {

                    textViewT1Total.setText("0");
                    textViewT2Total.setText("0");
                    Intent intent = new Intent(getApplicationContext(), WinnerActivity.class);
                    intent.putExtra("Winner Name", "Team One Is The Winner!");
                    intent.putExtra("Point", 1);
                    intent.putExtra("Winner One", player1Team1Uid);
                    intent.putExtra("Winner Two", player2Team1Uid);
                    intent.putExtra("Loser One", player1Team2Uid);
                    intent.putExtra("Loser Two", player2Team2Uid);
                    startActivity(intent);
                } else if (num2 >= 1001) {
                    textViewT1Total.setText("0");
                    textViewT2Total.setText("0");
                    Intent intent = new Intent(getApplicationContext(), WinnerActivity.class);
                    intent.putExtra("Winner Name", "Team Two Is The Winner!");
                    x = 2;
                    intent.putExtra("Point", 2);
                    intent.putExtra("Winner One", player1Team2Uid);
                    intent.putExtra("Winner Two", player2Team2Uid);
                    intent.putExtra("Loser One", player1Team1Uid);
                    intent.putExtra("Loser Two", player2Team1Uid);
                    startActivity(intent);
                }
                textViewT2Total.setText(String.valueOf(num2));


            }
        }

        if (requestCode == REQUEST_CODE_Points) {
            if (resultCode == RESULT_OK) {

                teamOnePoints = data.getIntExtra("Team One Points", 0);
                teamTwoPoints = data.getIntExtra("Team Two Points", 0);

                teamOneTotal = teamOneTotal + teamOnePoints;
                teamTwoTotal = teamTwoTotal + teamTwoPoints;

                int num = Integer.parseInt(textViewT1Total.getText().toString());
                num = num + teamOnePoints;
                textViewT1Total.setText(String.valueOf(num));

                int num2 = Integer.parseInt(textViewT2Total.getText().toString());
                num2 = num2 + teamTwoPoints;
                if (num >= 1001) {
                    textViewT1Total.setText("0");
                    textViewT2Total.setText("0");
                    Intent intent = new Intent(getApplicationContext(), WinnerActivity.class);
                    intent.putExtra("Winner Name", "Team One Is The Winner!");
                    x = 1;
                    intent.putExtra("Point", 1);
                    intent.putExtra("Winner One", player1Team1Uid);
                    intent.putExtra("Winner Two", player2Team1Uid);
                    intent.putExtra("Loser One", player1Team2Uid);
                    intent.putExtra("Loser Two", player2Team2Uid);

                    startActivity(intent);
                } else if (num2 >= 1001) {
                    textViewT1Total.setText("0");
                    textViewT2Total.setText("0");
                    Intent intent = new Intent(getApplicationContext(), WinnerActivity.class);
                    intent.putExtra("Winner Name", "Team Two Is The Winner!");
                    x = 2;
                    intent.putExtra("Point", 2);
                    intent.putExtra("Winner One", player1Team2Uid);
                    intent.putExtra("Winner Two", player2Team2Uid);
                    intent.putExtra("Loser One", player1Team1Uid);
                    intent.putExtra("Loser Two", player2Team1Uid);
                    startActivity(intent);
                }
                textViewT2Total.setText(String.valueOf(num2));
            }
        }
    }
    @Override
    public void onBackPressed() {
        exitByBackKey();
    }

    protected void exitByBackKey() {

        android.support.v7.app.AlertDialog alertbox = new android.support.v7.app.AlertDialog.Builder(this)
                .setMessage("Do you want to exit current game?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }
}
