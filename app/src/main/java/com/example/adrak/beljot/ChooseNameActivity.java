package com.example.adrak.beljot;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChooseNameActivity extends AppCompatActivity {

    private Button btnAccept;

    private Spinner playerOne;
    private Spinner playerTwo;
    private Spinner playerThree;
    private Spinner playerFour;

    private String player1Team1;
    private String player2Team1;
    private String player1Team2;
    private String player2Team2;

    private String player1Team1Uid;
    private String player2Team1Uid;
    private String player1Team2Uid;
    private String player2Team2Uid;

    private List<String> playerNamesArray = new ArrayList<String>();
    private HashMap<String, String> playerNamesMap = new HashMap<String, String>();

    Intent intent = new Intent();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference userDbReference = database.getReference("user");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_name);

        btnAccept = findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClick();
            }
        });
    }

    public void onStart() {
        super.onStart();

        playerOne = findViewById(R.id.playerOne);
        playerTwo = findViewById(R.id.playerTwo);
        playerThree = findViewById(R.id.playerThree);
        playerFour = findViewById(R.id.playerFour);

        userDbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                fillPlayerNames((Map<String, Object>) dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                doToast("The read failed: " + databaseError.getCode());
            }
        });
    }

    private void doToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void doToast2() {
        Toast.makeText(this, "You must select different display names", Toast.LENGTH_SHORT).show();

    }

    private void doToast3() {
        Toast.makeText(this, "You must select display names", Toast.LENGTH_SHORT).show();
    }

    private void onButtonClick() {

        player1Team1 = playerOne.getSelectedItem().toString();
        player1Team1Uid = playerNamesMap.get(player1Team1);

        player2Team1 = playerTwo.getSelectedItem().toString();
        player2Team1Uid = playerNamesMap.get(player2Team1);

        player1Team2 = playerThree.getSelectedItem().toString();
        player1Team2Uid = playerNamesMap.get(player1Team2);

        player2Team2 = playerFour.getSelectedItem().toString();
        player2Team2Uid = playerNamesMap.get(player2Team2);

        if (player1Team1 != "" && player2Team1 != "" && player1Team2 != "" && player2Team2 != "") {
            if (player1Team1 != player2Team1 && player1Team1 != player1Team2 && player1Team1 != player2Team2
                    && player2Team1 != player1Team2 && player2Team1 != player2Team2 && player1Team2 != player2Team2) {

                intent.putExtra("Team One Player One", player1Team1);
                intent.putExtra("Team One Player Two", player2Team1);
                intent.putExtra("Team Two Player One", player1Team2);
                intent.putExtra("Team Two Player Two", player2Team2);

                intent.putExtra("Team One Player One Uid", player1Team1Uid);
                intent.putExtra("Team One Player Two Uid", player2Team1Uid);
                intent.putExtra("Team Two Player One Uid", player1Team2Uid);
                intent.putExtra("Team Two Player Two Uid", player2Team2Uid);
                setResult(RESULT_OK, intent);
                finish();

            } else {
                doToast2();
            }
        } else {
            doToast3();
        }
    }

    private void fillPlayerNames(Map<String, Object> users) {
        playerNamesArray = new ArrayList<String>();
        playerNamesMap = new HashMap<String, String>();

        playerNamesArray.add("");

        for (Map.Entry<String, Object> entry : users.entrySet()) {

            Map singleUser = (Map) entry.getValue();

            playerNamesArray.add((String) singleUser.get("DisplayName"));
            playerNamesMap.put((String) singleUser.get("DisplayName"), entry.getKey());

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, playerNamesArray);
            playerOne.setAdapter(adapter);
            playerTwo.setAdapter(adapter);
            playerThree.setAdapter(adapter);
            playerFour.setAdapter(adapter);
        }
    }

    @Override
    public void onBackPressed() {
        exitByBackKey();
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to go cancel adding points?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        Intent intent=new Intent(ChooseNameActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }
}
