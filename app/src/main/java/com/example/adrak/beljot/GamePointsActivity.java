package com.example.adrak.beljot;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GamePointsActivity extends AppCompatActivity {
    private int tim1Poeni = 0;
    private int tim2Poeni = 0;

    private boolean teamOneBool;
    private boolean teamTwoBool;

    private EditText editTextT1;
    private EditText editTextT2;
    private Button btnAccept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_points);

        teamTwoBool = false;
        teamOneBool = false;
        editTextT1 = (EditText) findViewById(R.id.editTextT1);
        editTextT2 = (EditText) findViewById(R.id.editTextT2);
        btnAccept = (Button) findViewById(R.id.btnAcc);


        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                poeni();
            }
        });

        editTextT1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                teamOneBool = true;
                teamTwoBool = false;

                return false;
            }


        });
        editTextT2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                teamOneBool = false;
                teamTwoBool = true;

                return false;
            }


        });


        editTextT1.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                if (s.length() != 0 && editTextT1.getText().length() != 0 && teamOneBool == true && teamTwoBool == false)

                    if (Integer.parseInt(editTextT1.getText().toString()) == 252) {
                        editTextT2.setText("0");
                    }
                    else {

                        editTextT2.setText("" + (162 - Integer.parseInt(editTextT1.getText().toString())));

                    }
            }
        });

        editTextT2.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                if(s.length() != 0 && editTextT2.getText().length() != 0 && teamTwoBool == true && teamOneBool == false){
                    if (Integer.parseInt(editTextT2.getText().toString()) == 252) {
                        editTextT1.setText("0");
                    }
                    else {
                        editTextT1.setText("" + (162 - Integer.parseInt(editTextT2.getText().toString())));
                    }
                }
            }
        });
    }

        private void poeni(){

            tim1Poeni = Integer.parseInt(editTextT1.getText().toString().trim());
            tim2Poeni = Integer.parseInt(editTextT2.getText().toString().trim());

             if (tim1Poeni == 252 || tim2Poeni == 252) {

                Intent intent = new Intent();
                intent.putExtra("Team One Points", tim1Poeni);
                intent.putExtra("Team Two Points", tim2Poeni);
                setResult(RESULT_OK, intent);
                finish();
            }
             else if(tim1Poeni <0 || tim2Poeni <0)
            {
                Toast.makeText(GamePointsActivity.this, "One of the team points is negative", Toast.LENGTH_SHORT).show();

            }
             else  if ((tim1Poeni + tim2Poeni != 162)) {
                Toast.makeText(GamePointsActivity.this, "You've entered a wrong amount of points (162 Total)", Toast.LENGTH_SHORT).show();

            }
            else if ((tim1Poeni + tim2Poeni) == 162) {
                Intent intent = new Intent();
                intent.putExtra("Team One Points", tim1Poeni);
                intent.putExtra("Team Two Points", tim2Poeni);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
        @Override
        public void onBackPressed () {
            exitByBackKey();
        }

        protected void exitByBackKey () {

            AlertDialog alertbox = new AlertDialog.Builder(this)
                    .setMessage("Do you want to go back to Menu?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        // do something when the button is clicked
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {

                        // do something when the button is clicked
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    })
                    .show();
        }
}
