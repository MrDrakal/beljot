package com.example.adrak.beljot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    final Context context = this;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference userDbReference = database.getReference("user");
    private Button btnLogout;
    private Button btnNewGame;
    private Button btnLeaderboard;
    private Button btnChangeDisplayName;
    private List<UserModel> registeredUsers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnNewGame = (Button) findViewById(R.id.btnNewGame);
        btnLeaderboard = (Button) findViewById(R.id.btnLeaderboard);
        btnChangeDisplayName = (Button) findViewById(R.id.btnChangeDisplayName);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogout();
            }
        });

        btnLeaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartLeaderBoardActivity();
            }
        });

        FirebaseMessaging.getInstance().subscribeToTopic(firebaseAuth.getCurrentUser().getUid())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Subscribed to notifications";
                        if (!task.isSuccessful()) {
                            msg = "Failed to subscribe to notifications";
                        }

                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        btnNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (registeredUsers.size() < 4) {
                    doToast("Not enough players to start a game");
                    return;
                }

                StartNewGameActivity();
            }
        });

        btnChangeDisplayName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                LayoutInflater li = LayoutInflater.from(context);
                View inputDialogView = li.inflate(R.layout.input_dialog, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                alertDialogBuilder.setView(inputDialogView);

                final EditText userInput = (EditText) inputDialogView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setTitle("New display name?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, int id) {
                                        final String displayName = userInput.getText().toString();

                                        if (TextUtils.isEmpty(displayName)) {
                                            doToast("Display name cannot be empty");
                                            return;
                                        }

                                        if (displayName.length() < 6) {
                                            doToast("Display name must be more that 6 characters");
                                            return;
                                        }

                                        if (firebaseAuth.getCurrentUser() == null) {
                                            doToast("User is not logged in");
                                            return;
                                        }

                                        if (checkIfDisplayNameExists(displayName)) {
                                            doToast("Display name already exists");
                                            return;
                                        }

                                        userDbReference.child(firebaseAuth.getCurrentUser().getUid()).child("DisplayName").setValue(displayName);
                                        doToast("Display name changed");
                                    }
                                })
                        .

                                setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    private boolean checkIfDisplayNameExists(String displayName) {

        for (int i = 0; i < registeredUsers.size(); i++) {

            String temp = registeredUsers.get(i).DisplayName;
            if (Objects.equals(temp, displayName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        userDbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                fillUsersList((Map<String, Object>) dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                doToast("The read failed: " + databaseError.getCode());
            }
        });
        // Check if user is signed in (non-null) and update UI accordingly.
        final FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            final String username = currentUser.getEmail();
            userDbReference
                    .child(currentUser.getUid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.child("Username").getValue() == null) {
                                UserModel user = new UserModel();
                                int index = username.indexOf('@');
                                user.DisplayName = username.substring(0, index);
                                user.Username = username;
                                user.LostGames = new Long(0);
                                user.WonGames = new Long(0);
                                userDbReference.child(currentUser.getUid()).setValue(user);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            doErrorToast();
                        }
                    });
        }
    }

    private void doErrorToast() {
        Toast.makeText(this, "Something happened", Toast.LENGTH_SHORT).show();
    }

    private void doLogout() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(firebaseAuth.getCurrentUser().getUid())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Unsubscribe to notifications";
                        if (!task.isSuccessful()) {
                            msg = "Failed to Unsubscribe to notifications";
                        }

                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        firebaseAuth.signOut();
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }

    private void doToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void fillUsersList(Map<String, Object> users) {
        registeredUsers = new ArrayList<>();
        for (Map.Entry<String, Object> entry : users.entrySet()) {

            Map singleUser = (Map) entry.getValue();
            UserModel registeredUser = new UserModel();

            registeredUser.userUid = (String) entry.getKey();
            registeredUser.Username = (String) singleUser.get("Username");
            registeredUser.DisplayName = (String) singleUser.get("DisplayName");
            registeredUser.LostGames = (Long) singleUser.get("LostGames");
            registeredUser.WonGames = (Long) singleUser.get("WonGames");

            registeredUsers.add(registeredUser);
        }
    }

    private void StartNewGameActivity() {
        startActivity(new Intent(this, NewGameActivity.class));
    }

    private void StartLeaderBoardActivity() {
        startActivity(new Intent(this, LeaderboardActivity.class));
    }

    @Override
    public void onBackPressed() {
        exitByBackKey();
    }

    protected void exitByBackKey() {

        android.support.v7.app.AlertDialog alertbox = new android.support.v7.app.AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        finishAffinity();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

}