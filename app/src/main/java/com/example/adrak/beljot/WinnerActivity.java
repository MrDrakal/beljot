package com.example.adrak.beljot;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class WinnerActivity extends AppCompatActivity {
    public int point = 0;
    Button btnNewMatch;
    Button btnGoToMenu;
    TextView textViewWinner;
    private RequestQueue queue;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference userDbReference = database.getReference("user");
    Long points = new Long(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);

        queue = Volley.newRequestQueue(WinnerActivity.this);

        btnNewMatch = (Button) findViewById(R.id.btnNewMatch);
        btnGoToMenu = (Button) findViewById(R.id.btnGoToMenu);

        String winner = getIntent().getStringExtra("Winner Name");
        point = getIntent().getIntExtra("Point", 0);
        textViewWinner = (TextView) findViewById(R.id.textViewWinner);

        // Send Notification to firebase
        try {
            JSONObject winnerNotification = new JSONObject();
            winnerNotification.put("body", "You have won the game.");
            winnerNotification.put("title", "Won");
            winnerNotification.put("sound", "on");
            winnerNotification.put("priority", "high");

            JSONObject loserNotification = new JSONObject();
            loserNotification.put("body", "You have lost the game.");
            loserNotification.put("title", "Lost");
            loserNotification.put("sound", "on");
            loserNotification.put("priority", "high");

            String winnerOne = getIntent().getStringExtra("Winner One");
            String winnerTwo = getIntent().getStringExtra("Winner Two");
            String loserOne = getIntent().getStringExtra("Loser One");
            String loserTwo = getIntent().getStringExtra("Loser Two");

            updateUserData(winnerOne, true);
            updateUserData(winnerTwo, true);
            updateUserData(loserOne, false);
            updateUserData(loserTwo, false);

            sendNotifcation(winnerOne, winnerNotification);
            sendNotifcation(winnerTwo, winnerNotification);
            sendNotifcation(loserOne, loserNotification);
            sendNotifcation(loserTwo, loserNotification);

        } catch (Exception e) {
            doToast("Notification Error: " + e.getMessage());
        }

        btnNewMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(WinnerActivity.this, NewGameActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });
        btnGoToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WinnerActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        textViewWinner.setText(winner);
    }

    private void sendNotifcation(String userUid, JSONObject notificationData) {
        try {

            JSONObject params = new JSONObject();
            params.put("notification", notificationData);
            params.put("to", "/topics/" + userUid);

            JsonObjectRequest req =
                    new JsonObjectRequest(
                            "https://fcm.googleapis.com/fcm/send",
                            params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        Log.d("Notification Success", "Success: " + response.getString("message_id"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    doToast("Error: " + error.getMessage());
                                }
                            }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap();
                            params.put("Content-Type", "application/json");
                            params.put("authorization", "key=" + "AAAAoayyK_g:APA91bGNpl8EUHtyLTbBRHzFTiUkzzJLRHWqAc5bWUkbdXoh4IZ9zhRzf8ASVYgjSqlxepmVZroxgTgdAm5sTR9AjIly_ZFTM_CbWe1FTrbKG6IHzALQb7pNcwiI4uk6k3ScpoX43bPp");
                            return params;
                        }
                    };

            queue.add(req);

        } catch (Exception e) {
            doToast("Notification Error: " + e.getMessage());
        }
    }

    private void doToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void updateUserData(final String userUid, boolean hasWon) {
        try {
            final DatabaseReference userUidDbReference = userDbReference.child(userUid);
            points = new Long(0);

            if (hasWon) {
                userUidDbReference.child("WonGames").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Long tempPoints = (Long) dataSnapshot.getValue();
                        points = tempPoints;

                        points++;

                        userUidDbReference.child("WonGames").setValue(points);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        doToast(databaseError.getMessage());
                    }
                });
                return;
            }

            userUidDbReference.child("LostGames").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Long tempPoints = (Long) dataSnapshot.getValue();
                    points = tempPoints;

                    points++;

                    userUidDbReference.child("LostGames").setValue(points);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    doToast(databaseError.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}



