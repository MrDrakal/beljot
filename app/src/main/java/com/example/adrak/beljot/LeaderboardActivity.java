package com.example.adrak.beljot;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class LeaderboardActivity extends AppCompatActivity {

    ListView leaderboardList;
    List<UserModel> leaderboardArraylist = new ArrayList<>();
    ArrayList<String> leaderboardArrayString;
    ArrayAdapter<String> leaderboardArrayAdapter;
    ProgressBar progressBar;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference userDbReference = database.getReference("user");
    String sortBy;

    // Enum Values
    // SortWonGamesFromHigherToLower
    // SortWonGamesFromLowerToHigher
    // SortLostGamesFromHigherToLower
    // SortLostGamesFromLowerToHigher

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);

        sortBy = "SortWonGamesFromHigherToLower";

        leaderboardList = (ListView) findViewById(R.id.leaderboardList);
        leaderboardList.setEnabled(false);
        leaderboardArrayString = new ArrayList<>();
        leaderboardArrayAdapter = new ArrayAdapter<String>(this, R.layout.leaderboard_item, R.id.itemtext, leaderboardArrayString);
        leaderboardList.setAdapter(leaderboardArrayAdapter);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        userDbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                leaderboardArraylist.clear();
                leaderboardArrayString.clear();

                fillLeaderboardUsers((Map<String, Object>) dataSnapshot.getValue());
                sortLeaderboard();

                leaderboardArrayString.add("Top 10 Players");
                leaderboardArrayString.add(" ");

                for (int i = 0; i < leaderboardArraylist.size(); i++) {

                    if (i == 10) {
                        break;
                    }

                    UserModel data = leaderboardArraylist.get(i);
                    leaderboardArrayString.add(Integer.toString(i + 1) + ":" + data.DisplayName + " - Wins: " + data.WonGames.toString() + " Loses: " + data.LostGames.toString());
                }

                leaderboardArrayAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                doToast("The read failed: " + databaseError.getCode());
            }
        });
    }

    private void fillLeaderboardUsers(Map<String, Object> users) {
        leaderboardArraylist = new ArrayList<>();
        for (Map.Entry<String, Object> entry : users.entrySet()) {

            Map singleUser = (Map) entry.getValue();
            UserModel registeredUser = new UserModel();

            registeredUser.userUid = (String) entry.getKey();
            registeredUser.Username = (String) singleUser.get("Username");
            registeredUser.DisplayName = (String) singleUser.get("DisplayName");
            registeredUser.LostGames = (Long) singleUser.get("LostGames");
            registeredUser.WonGames = (Long) singleUser.get("WonGames");

            leaderboardArraylist.add(registeredUser);
        }
    }

    private void doToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void sortLeaderboard() {

        if (sortBy == "SortWonGamesFromLowerToHigher") {
            // Sort by WonGames from lower to higher
            Collections.sort(leaderboardArraylist, new Comparator<UserModel>() {
                @Override
                public int compare(UserModel o1, UserModel o2) {
                    return o1.WonGames.compareTo(o2.WonGames);
                }
            });
            return;
        }

        if (sortBy == "SortLostGamesFromHigherToLower") {
            // Sort by LostGames from higher to lower
            Collections.sort(leaderboardArraylist, new Comparator<UserModel>() {
                @Override
                public int compare(UserModel o1, UserModel o2) {
                    return o2.LostGames.compareTo(o1.LostGames);
                }
            });
            return;
        }

        if (sortBy == "SortLostGamesFromLowerToHigher") {
            // Sort by LostGames from lower to higher
            Collections.sort(leaderboardArraylist, new Comparator<UserModel>() {
                @Override
                public int compare(UserModel o1, UserModel o2) {
                    return o1.LostGames.compareTo(o2.LostGames);
                }
            });
            return;
        }

        // Sort by WonGames from higher to lower
        Collections.sort(leaderboardArraylist, new Comparator<UserModel>() {
            @Override
            public int compare(UserModel o1, UserModel o2) {
                return o2.WonGames.compareTo(o1.WonGames);
            }
        });
    }
}
